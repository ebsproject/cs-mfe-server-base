# Core System Micro-frontend Server Base Image Dockerfile

### Prerequisites

#### User Account and Access
1. DockerHub account (with access to the image repository)

### Building the image
[1] Clone this repository. Navigate into `cs-mfe-server-base`
```bash
git clone git@bitbucket.org:ebsproject/cs-mfe-server-base.git
cd cs-mfe-server-base
```

[2] Update the Dockerfile with the proper configurations.
- ```baseImage``` should be set to `ebsproject/cs-mfe-server-base`
- ```sourceDir``` directory where in the front-end assets are located
- ```libVersion``` current version of the assets
- ```libMainFile``` the main file accessed by the server
- ```importMapFilename``` the manifest file; defaults to app.importmap

[2.1] You can also provide these values without manually modifying the Dockerfile
```bash
docker build --build-arg libVersion=21.08.06
```

[3] Export the necessary environment variables
```bash
e.g.
export ENVIRONMENT_NAME=dev
export DOCKER_ORG_NAME=ebsproject
```

[4] Build the Docker image and push it into a target repository
```bash
docker build -f Dockerfile . -t $DOCKER_ORG_NAME/<image_name>
docker push $DOCKER_ORG_NAME/<image_name>
```

[5] Run the newly created image
```bash
docker run -dp 3000:80 $DOCKER_ORG_NAME/<image_name>
```

[6] Verify if the image container is running then navigate into the container
```bash
docker ps
docker exec -it $CONTAINER_ID bash
```

[7] Navigate into /usr/share/nginx/html. The app.importmap and $VIRTUALDIR folder should be seen.
```bash
cd /usr/share/nginx/html
```

[8] Check if the contents of the file and the folder are correct
```bash
ls ${virtualDir}/${libVersion} # This should contain the content of the MFE repository
cat app.importmap # Contains the mappings of the files inside the MFE repository


# Sample:
{ 
    "imports": { 
        "21.08.26": "ebsproject.org/ebsproject/21.08.26/ebs-styleguide.js", 
        "21.08.26/": "ebsproject.org/ebsproject/21.08.26/" 
    }, 
    "scopes": {}
}
```
